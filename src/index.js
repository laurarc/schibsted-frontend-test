import { Accordion } from './components/accordion/accordion';

const accordionUI = new Accordion();

document.addEventListener('DOMContentLoaded', () => {
  console.log("DOM fully loaded and parsed ;)");
  accordionUI.init();
});
